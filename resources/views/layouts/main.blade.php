<!DOCTYPE html>
<html>

    @include('partials.head')

<body class="hold-transition skin-blue sidebar-mini">

    <div class="wrapper">

        @include('partials.header')

        @include('partials.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            @yield('content')

        </div>
        <!-- /.content-wrapper -->

        @include('partials.footer')

{{--        @include('partials.control-sidebar')--}}

    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap 3.3.7 -->
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Slimscroll -->
    <script src="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="/dist/js/adminlte.min.js"></script>
    {{--<!-- AdminLTE dashboard demo (This is only for demo purposes) -->--}}
    {{--<script src="/dist/js/pages/dashboard.js"></script>--}}
    {{--<!-- AdminLTE for demo purposes -->--}}
    {{--<script src="/dist/js/demo.js"></script>--}}

    {{--@yield('footer-inject')--}}
    @stack('scripts')

</body>
</html>
