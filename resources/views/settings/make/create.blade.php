@extends('layouts.main')

@section('head-inject')
  <!-- DataTables -->
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Producator
      <small>Adauga producator</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Acasa</a></li>
      <li><a href="#">Setari</a></li>
      <li><a href="#">Producatori</a></li>
      <li class="active">Adauga producator</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Producator nou</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="POST" action="{{ route('make.store') }}">
            {{ csrf_field() }}
            <div class="box-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              <div class="form-group">
                <label for="inputPlate">Producator</label>
                <input type="text" class="form-control" id="inputName" placeholder="Audi" name="name" required>
              </div>
{{--              <div class="form-group">--}}
{{--                <label for="inputDescription">Descriere</label>--}}
{{--                <textarea id="inputDescription" class="form-control" rows="3" placeholder="Descriere ..." name="description"></textarea>--}}
{{--              </div>--}}
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Salveaza</button>
            </div>
          </form>
        </div>
      </div>
        <!-- /.box -->

    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

@section('footer-inject')

@endsection
