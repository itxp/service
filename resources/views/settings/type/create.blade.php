@extends('layouts.main')

@section('head-inject')
  <!-- DataTables -->
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Modele
      <small>Adauga model</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Acasa</a></li>
      <li><a href="#">Setari</a></li>
      <li><a href="#">Model</a></li>
      <li class="active">Adauga model</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Model nou</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="POST" action="{{ route('type.store') }}">
            {{ csrf_field() }}
          <input type="hidden" id="make_id" name="make_id" @if(old('make_id'))value="{{ old('make_id') }}"@endif>

            <div class="box-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              <div class="form-group">
                  <div class="dropdown">
                      <button id="make-button" class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">@if(old('make_name')) {{ old('make_name') }} @else *Selecteaza producator @endif
                          <span class="caret"></span></button>
                      <ul class="dropdown-menu" id="make-select">
                          <input class="form-control" id="myInput" type="text" placeholder="Cauta...">

                          @foreach($makes as $make)
                              <li id="{{ $make->id }}"><a href="#">{{ $make->name }}</a></li>
                          @endforeach

                      </ul>
                  </div>
              </div>
              <div class="form-group">
                <label for="inputPlate">Model</label>
                <input type="text" class="form-control" id="inputName" placeholder="A4" name="name" required>
              </div>
{{--              <div class="form-group">--}}
{{--                <label for="inputDescription">Descriere</label>--}}
{{--                <textarea id="inputDescription" class="form-control" rows="3" placeholder="Descriere ..." name="description"></textarea>--}}
{{--              </div>--}}
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Salveaza</button>
            </div>
          </form>
        </div>
      </div>
        <!-- /.box -->

    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

@section('footer-inject')

@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $("#myInput").on("keyup", function() {
                // console.log('ok');
                var value = $(this).val().toLowerCase();
                $(".dropdown-menu li").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
            $("#make-select li").click(function() {
                $("#make_id").val(this.id);
                $("#make_name").val($(this).find("a").text());
                $("#make-button").text($(this).find("a").text());
            });
        });
    </script>
@endpush
