<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;


class DeployController extends Controller
{
    //
    public function deploy(Request $request)
    {
        $githubPayload = $request->getContent();
        Log::info('payload='.$githubPayload);

        $githubHash = $request->header('X-Hub-Signature');
        Log::info('hash='.$githubHash);

        $localToken = config('app.deploy_secret');
        Log::info('token='.$localToken);

        $localHash = 'sha1=' . hash_hmac('sha1', $githubPayload, $localToken, false);
        Log::info('localhash='.$localHash);

//        if (hash_equals($githubHash, $localHash)) {
            $root_path = base_path();
            $process = new Process('cd ' . $root_path . '; ./deploy.sh');
            $process->run(function ($type, $buffer) {
                Log::info('buffer='.$buffer);
                echo $buffer;
            });
//        }
    }
}
